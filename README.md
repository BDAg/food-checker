<h2>FOOD CHECKER</h2>

<h5>O FOOD CHECKER é um site criado para ajudar pessoas de classe baixa e média, cujo objetivo é de se planejar economicamente para as compras do mercado.</h5>

<h5>É um site que faz o levantamento de alguns itens da cesta básica em diferentes mercados para exibir seus preços e o histórico, diferente dos sites convencionais de mercados.</h5>

<h5>O nosso produto busca facilitar o acesso às informações e também diminuir o tempo gasto na procura de produtos em sites convencionais.</h5>

**Trabalho realizado por:**
<ul>
<li>DAVI MASSINI MALIMPENSA</li>
<li>GABRIEL VALE CHAVES</li>
<li>GUSTAVO HENRIQUE KENZO OLIVEIRA</li>
<li>JEAN CARLOS DE ANDRADE VIEIRA</li>
<li>RICARDO ALEXANDRE FERREIRA</li>
</ul>
